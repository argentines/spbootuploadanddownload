package es.skuguk.filedowloadupload.resource;

import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import static java.nio.file.Files.copy;
import static java.nio.file.Paths.get;
import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;
import static org.springframework.http.HttpHeaders.CONTENT_DISPOSITION;

@RestController
@RequestMapping("/file")
public class FileResource {

    //Define a location
    public static final String DIRICTORY = System.getProperty("user.home") + "/Download/uploads/";

    //Define a method to upload to upload files
    @PostMapping("/upload")
    public ResponseEntity<List<String>> uploadFiles(@RequestParam("files") List<MultipartFile> multipartFile) throws IOException {
        List<String> filenames = new ArrayList<>();
        for(MultipartFile file: multipartFile) {
            String filename = StringUtils.cleanPath(file.getOriginalFilename());
            Path fileStorage = get(DIRICTORY, filename).toAbsolutePath().normalize();
            copy(file.getInputStream(), fileStorage, REPLACE_EXISTING);
            filenames.add(filename);
        }

        return ResponseEntity.ok().body(filenames);
    }

    //Define a method to download files
    @GetMapping("/download/{filename}")
    public ResponseEntity<Resource> downloadFiles(@PathVariable("filename") String filename) throws IOException {
        Path filePath = get(DIRICTORY).toAbsolutePath().normalize().resolve(filename);
        if(!Files.exists(filePath)) {
            throw new FileNotFoundException(filename + " Was not found on the server");
        }

        Resource resource = new UrlResource(filePath.toUri());
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("File-Name", filename);
        httpHeaders.add(CONTENT_DISPOSITION, "attachment;File-Name=" + resource.getFilename());

        return ResponseEntity.ok()
                             .contentType(MediaType.parseMediaType(Files.probeContentType(filePath)))
                             .headers(httpHeaders).body(resource);
    }

    //Get All Files
    @GetMapping("/files-list")
    public ResponseEntity<List<String>> getAllFiles()  {
        List<String> filenames = new ArrayList<>();
        File dir = new File(DIRICTORY);

        if(dir.exists()) {
                File[] files = dir.listFiles();
                for(File file: files) {
                    if(!file.isDirectory()) {
                        if (file.isFile()) {
                            filenames.add(file.getName());
                        }
                    }
                }
        }
        return ResponseEntity.ok().body(filenames);
    }

}
